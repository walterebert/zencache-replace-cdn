<?php
/**
 * Plugin Name: ZenCache replace CDN
 * Description:  Replace external resources with local files for the ZenCache WordPress plugin
 * Plugin URI: https://gitlab.com/walterebert/zencache-replace-cdn
 * Author: Walter Ebert
 * Author URI: http://walterebert.com/
 * Version: 1.3.4
 * License: GPLv2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @package WordPress
 * @subpackage ZenCache
 */

define( 'ZENCACHE_REPLACE_CDN_VERSION', '1.3.3' );

if ( is_admin() && filter_input( INPUT_GET, 'page', FILTER_SANITIZE_URL ) === 'zencache' ) {
	add_action(
		'admin_enqueue_scripts',
		function() {
			if ( ! wp_style_is( 'font-awesome' )  ) {
				wp_enqueue_style( 'font-awesome', plugins_url( 'css/font-awesome.min.css', __FILE__ ), [], '4.5.0' );
			}
			wp_enqueue_style( 'sharkicons', plugins_url( 'zencache/submodules/sharkicons/styles.min.css' ), [], ZENCACHE_REPLACE_CDN_VERSION );
			wp_enqueue_style( 'zencache', plugins_url( 'css/menu-pages.min.css', __FILE__ ), [], ZENCACHE_REPLACE_CDN_VERSION );
		}
	);
}
