default: data
	@mkdir -p css fonts
	@cd $< && git remote update && git checkout master
	@cp -f $</css/* ./css
	@cp -f $</fonts/* ./fonts
	@npm install
	@uglify -c -s css/menu-pages.css -o css/menu-pages.min.css
	@du -bh css* font*

data:
	@git clone https://github.com/FortAwesome/Font-Awesome.git $@

.PHONY: default
