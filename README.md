# Zencache replace CDN
Replace external resources with local files for the [ZenCache](https://wordpress.org/plugins/zencache/) WordPress plugin.

ZenCache has been renamed to [Comet Cache](https://wordpress.org/plugins/comet-cache/). Comet Cache does not load external resources, so this plugin adds no value to Comet Cache.
